// express server
var express = require('express')();
// json body parser
var bodyParser = require('body-parser');
express.use(bodyParser.json());
express.use(bodyParser.urlencoded({extended: false}));
var cookieParser = require('cookie-parser');
express.use(cookieParser());
// logger
var logger = require('morgan');
// CORS
var cors = require('cors');
express.use(cors({
  origin: 'http://projgw.cse.cuhk.cuhk:2874'
, credentials: true
}));

var config = require('./config');
var server = require('http').createServer(express);
var port = process.env.PORT || 8080;
var host = config.expressServer.host;
server.listen(port, host, function() {
  console.log('Node js server is running at' + host + ':' + port);
});
if (express.get('env') !== 'development') {
  express.use(logger('combined'));
} else {
  express.use(logger('dev'));
}

// socket io
var io = require('socket.io')(server, {path: '/api/ws'});
//var socket = require('./socket');
//socket.listen(io);
var ecgSocket = require('./socketio/ecg');
ecgSocket.init(io);

var apiRouter = require('./route/api');
express.use('/api/test', apiRouter);

var userRoute = require('./route/user');
express.use('/api/user', userRoute);

var doctorRoute = require('./route/doctor');
express.use('/api/doctor', doctorRoute);

var patientRoute = require('./route/patient');
express.use('/api/patient', patientRoute);

var ecgRoute = require('./route/ecg');
express.use('/api/ecg', ecgRoute);

// express server error handler
var errorhandler = require('./middleware/errorhandler');
express.use(errorhandler);
