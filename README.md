# README #

This is the server side of heart monitoring application.


### What is this repository for? ###

* This is the server side application for analyse, record, monitor patient's heart)

------
### Requirement ###
1. nodejs v6
2. npm
3. apidoc (for generate doc)
4. python 2.7, with numpy, scipy

------
### Server deployment ###

1. Pm2 command

* Restart all Application
```bash
pm2 restart all
```
*  Show all application status
```bash
pm2 list
```
*  Reload all configuration
``` bash
pm2 reload process.json
```

2 Install Dependencies
``` bash
npm install
```
* Add new Dependencies
``` bash
npm install dev -S
```
3 API documentation  
Use apidoc for generation of documentation

[SYNTAC Link](http://apidocjs.com/#examples)

* Generate apidoc
```bash
apidoc -i /route -i /socketio -o doc/
```

------
### Coding Style ###
1. nodejs javascript
Install eslintrc and load the eslintrc
