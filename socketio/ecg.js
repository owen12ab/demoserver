
var socketi = {};
var verifySocketToken = require('../middleware/verifySockioToken');
var doctor = require('../model/doctor');
var zerorpc = require('zerorpc');
var rawEcg = require('../model/rawEcg');
var ecgModel = require('../model/ecg');
var msgModel = require('../model/msg');

socketi.init = function(io) {
  var redis = require('socket.io-redis');
  if (process.env.NODE_ENV === 'prod') {
    io.adapter(redis({host: '127.0.0.1', port: 6379}));
  }
  io.use(require('socket.io-cookie-parser')());
  io.use(verifySocketToken);
  client = new zerorpc.Client();
  client.connect('tcp://127.0.0.1:4001');
  io.on('connection', function(socket) {
    if (!socket.request.token) {
      console.log('auth error disconnect user');
      return socket.disconnect(true);
    }
    if (socket.request.token.type === 'patient') {
      socket.join(socket.request.token.pid, function(err) {
        if (err) {
          return socket.disconnect(true);
        }
      });
    } else {
      socket.join(socket.request.token.pid);
      doctor.getAllPatient(socket.request.token.pid, function(err, res) {
        if (!err) {
          res.forEach(function(re) {
            socket.join(re.patientid);
          });
        }
      });
    }
    socket.on('disconnect', function() {
      console.log('socketio disconnect');
    });
/**
 * @api {socket.io} /api/ws emit(echo)
 * @apiName Echo test
 * @apiGroup ioCommand
 *
 * @apiExample {Socketio} Example usage:
 *     socket.emit("echo")
 * @apiSuccess Emit echo event
 *
 */
/**
 * @api {socket.io} /api/ws on(echo)
 * @apiName Echo test
 * @apiGroup ioEvent
 * @apiExample {Socketio} Example usage:
 *     socket.on("echo", callback)
 *
 */
    socket.on('echo', function() {
      console.log('echo');
      socket.to(socket.request.token.pid).emit('echo');
    });
/**
 * @api {socket.io} /api/ws emit(newEcg)
 * @apiName New Ecg to Server only for analyse purpose,
   must have at least two wave
 * @apiGroup ioCommand
 *
 * @apiParam {String} data Raw Ecg data with format data+'--'+ecgData
 * ecgData is for 3 digit hex for each number
 * @apiExample {Socketio} Example usage:
 *     socket.emit('newEcg', '2016/10/11 11:11:11.000--00a00b00c)
 */
/**
 * @api {socket.io} /api/ws on(newEcg)
 * @apiName New Ecg Receive from server
 * @apiGroup ioEvent
 *
 * @apiSuccess {String} raw raw Ecg data ie a12a13a14
 * @apiSuccess {String} pid Pid of patient
 * @apiExample {Socketio} Example usage:
 *     socket.on('newEcg', (data) => {
 *       console.log(data.pid);
 *       console.log(data.raw);
 *     })
 *
 */
/**
 * @api {socket.io} /api/ws on(ecgAnalyseEcg)
 * @apiName New Ecg analyse from server
 * @apiGroup ioEvent
 *
 * @apiSuccess {Array} RawEcgAnalyseDat Ecg analyse data [RR, RR, RR]
 * @apiExample {Socketio} Example usage:
 *     socket.on('ecgAnalyseEcg', (data) => {
 *       if (data.status === 'success') {
 *         console.log(data.result);
 *       } else { failt }
 *     })
 */
    socket.on('newEcg', function(data) {
      var pid = socket.request.token.pid || 1;
      var ecgRaw = data.split('--');
      var receiveDate = new Date(ecgRaw[0]);
      rawEcg.appendToFile(pid, receiveDate, data + '\n', function(err) {
        if (err) {
          console.log('error writing on file', err);
        }
      });
      ecgRaw = ecgRaw[ecgRaw.length - 1];
      //io.to(pid).emit('newEcg', {pid: pid, raw:ecgRaw[1]});
      client.invoke('analyseEcg', ecgRaw, function(err, res){
        if (err || !res || res.length <= 0) {
          return;
        }
        if (res.length >= 2) {
          //data  = convertPythonEcg(res);
          var d = receiveDate;
          var rrs = [];
          for (var i = 0; i < res.length - 1; i++) {
            var rr = res[i + 1] - res[i];
            rrs.push(rr);
            d.setMilliseconds(d.getMilliseconds() + rr * 10);
            var tmpDate = new Date(d);
            ecgModel.addRR(pid, rr, tmpDate, function(err) {
              if (err) {
                console.log(err);
              }
            });
          }
          io.to(pid).emit('ecgAnalyseData', {status: 'success',
            timestamp: receiveDate,
            pid: socket.request.token.pid,
            result:rrs});
        } else {
          io.to(pid).emit('ecgAnalyseData', {status: 'fail'});
        }
        //console.log(err);
      });
    });
/*eslint-disable */
/**
 * @api {socket.io} /api/ws emit(relayEcg)
 * @apiName New Ecg to Server to other client and save to file but without analyse
 * @apiGroup ioCommand
 *
 * @apiParam {String} data Raw Ecg data with format data+'--'+ecgData
 * @apiExample {Socketio} Example usage:
 *     socket.emit('relayEcg', '2016/10/11 11:11:11.000--aa0aa1aa2aa4)
 */
 /*eslint-enable */
    socket.on('relayEcg', function(data) {
      var pid = socket.request.token.pid || 1;
      var ecgRaw = data.split('--');
      if (ecgRaw.length < 2) {
        return;
      }
      var receiveDate = new Date(ecgRaw[0]);
      if (!receiveDate) {
        return;
      }
      rawEcg.appendToFile(pid, receiveDate, data + '\n', function(err) {
        if (err) {
          console.log('error writing on file', err);
        }
      });
      io.to(pid).emit('newEcg', {pid: pid, raw:'' + ecgRaw[1]});
    });
  /*eslint-disable */
  /**
   * @api {socket.io} /api/ws emit(newMsg)
   * @apiName New Msg to server
   * @apiGroup ioCommand
   *
   * @apiParam {String} newMsg New msg from user
   * @apiParam {String} toPid receiver user id
   * @apiExample {Socketio} Example usage:
   *     socket.emit('newMsg', {toPid: "1", newMsg: 'New Msg'})
   */
  /**
   * @api {socket.io} /api/ws on(newMsg)
   * @apiName New Ecg from user
   * @apiGroup ioEvent
   *
   * @apiSuccess {String} msg raw Ecg message
   * @apiSuccess {String} pid Pid of patient
   * @apiSuccess {Date} time time of the msg receive
   * @apiExample {Socketio} Example usage:
   *     socket.on('newMsg', (data) => {
   *       console.log(data.pid);
   *       console.log(data.time);
   *       console.log(data.msg);
   *     })
   *
   */
   /*eslint-enable */
    socket.on('newMsg', function(data) {
      var time = new Date();
      var pid = socket.request.token.pid || 1;
      var msg = data.newMsg;
      var toPid = data.toPid;
      msgModel.save(pid, toPid, time, msg, function() {
      });
      io.to(toPid).emit('newMsg', {pid: pid, msg: msg, time: time});
    });
  });
};

module.exports = socketi;
