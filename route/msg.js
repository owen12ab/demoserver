var app = require('express').Router(); //eslint-disable-line new-cap

var verifyToken = require('../middleware/verifyToken');
var msgModel = require('../model/msg');
/**
 * @api {post} /api/msg/all Get all message
 * @apiName Get All Message
 * @apiGroup Message
 *
 * @apiSuccess {String} fromPid Send from user
 * @apiSuccess {String} toPid  Send to user
 * @apiSuccess {Date} mdate  Date receive the msg
 * @apiSuccess {String} content  Body of the msg
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "fromPid": "1",
 *       "toPid": "2",
 *       "mdate": "1900-12-12 12:12:12.121",
 *     }]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The old password is incorrect
 or user doesnt exist anymore.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
app.get('/all', verifyToken, function(req, res, next) {
  msgModel.getAll(req.user.pid, function(err, result) {
    if (err) {
      return next(err);
    }
    return res.send(result);
  });
});

/**
 * @api {post} /api/msg/with/:pid Get msg relate to pid
 * @apiName Get Pid Message
 * @apiGroup Message
 *
 * @apiSuccess {String} fromPid Send from user
 * @apiSuccess {String} toPid  Send to user
 * @apiSuccess {Date} mdate  Date receive the msg
 * @apiSuccess {String} content  Body of the msg
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "fromPid": "1",
 *       "toPid": "2",
 *       "mdate": "1900-12-12 12:12:12.121",
 *     }]
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The old password is
 incorrect or user doesnt exist anymore.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
app.get('/with/:pid(\\d+)', verifyToken, function(req, res, next) {
  msgModel.getPid(req.user.pid, req.params.pid, function(err, result) {
    if (err) {
      return next(err);
    }
    return res.send(result);
  });
});

module.exports = app;
