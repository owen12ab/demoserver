
// express router

var app = require('express').Router(); //eslint-disable-line new-cap

var verifyToken = require('../middleware/verifyToken');
var permission = require('../model/permission');
var ecgModel = require('../model/ecg');
var rawEcg = require('../model/rawEcg');

/*eslint-disable */
/**
 * @api {post} /api/ecg/allData/:patientid Get All analyse Data
 * @apiName Get All analyse Data
 * @apiGroup Ecg
 *
 * @apiHeader {String} patientid Users id of patient
 *
 * @apiSuccess (200) {Array} EcgData Array of ecg data
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden User dont have access right to patientid's ecg data
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */
app.post('/allData/:patientid(\\d+)',
  verifyToken,
  permission.checkPidToDid(function(req, res, next) {
    // check field completenesss
    ecgModel.getByPid(req.params.patientid, function(err, results) {
      if (err) {
        return next(err);
      }
      return res.send(results);
    });
  })
);
/*eslint-disable */
/**
 * @api {post} /api/ecg/analyseData/:patientid Get Specific analyse Data
 * @apiName Get Specific analyse Data
 * @apiGroup Ecg
 *
 * @apiHeader {String} patientid Users id of patient
 * @apiParam {DateTime} startTime starting time
 * #apiParam {DateTime} endTime ending time
 *
 * @apiSuccess (200) {Array} EcgData Array of ecg data
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden User dont have access right to patientid's ecg data
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */
app.post('/analyseData/:patientid(\\d+)',
  verifyToken,
  permission.checkPidToDid(function(req, res, next) {
    // check field completenesss
    if (!req.body.startTime || !req.body.endTime) {
      return res.sendStatus(400);
    }
    var startTime = new Date(req.body.startTime);
    var endTime = new Date(req.body.endTime);
    if (!startTime || !endTime) {
      return res.sendStatus(400);
    }
    ecgModel.getByPidDate(req.params.patientid,
      startTime, endTime,
      function(err, results) {
        if (err) {
          return next(err);
        }
        return res.send(results);
      }
    );
  })
);
/*eslint-disable */
/**
 * @api {post} /api/ecg/avgDat/:patientid Get Avg Stat
 * @apiName Get Avg Stat
 * @apiGroup Ecg
 *
 * @apiHeader {String} patientid Users id of patient
 *
 * @apiSuccess (200) {Array} EcgData Array of stat Data
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden User dont have access right to patientid's ecg data
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */

app.post('/avgData/:patientid(\\d+)', verifyToken,
  permission.checkPidToDid(function(req, res) {
    ecgModel.getAvgByPid(req.params.patientid, function(err, results) {
      if (err) {
        return next(err);
      }
      return res.send(results[0]);
    });
  })
);

/*eslint-disable */
/**
 * @api {post} /api/ecg/rawFile/:patientid Get Raw Ecg Data
 * @apiName Get Raw Ecg Data
 * @apiGroup Ecg
 *
 * @apiHeader {String} patientid Users id of patient
 * @apiParam {String} dateTime The sepecific that of ecg data in javascript data parser format
 *
 * @apiSuccess (200) {RAW} EcgData Raw Data of ECG, same format of socket io.emit(newEcg) data
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden User dont have access right to patientid's ecg data
 * @apiError (404) Forbidden The File for that data is not exist
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */
app.post('/rawFile/:patientid(\\d+)', verifyToken,
  permission.checkPidToDid(function(req, res, next) {
    if (!req.body.dateTime) {
      return res.sendStatus(400);
    }
    var d = new Date(req.body.dateTime);
    if (d === NaN) {
      return res.sendStatus(400);
    }
    rawEcg.getRawByDate(req.params.patientid, d, function(err, stats, name) {
      if (err || !stats.isFile()) {
        return next(err);
      }
      res.sendFile('/home/wmh1604/node_server/' + name.substr(1));
    });
  })
);
/*eslint-disable */
/**
 * @api {post} /api/ecg/rawEcgList/:patientid Get Raw Ecg List
 * @apiName Get Ecg List within time range
 * @apiGroup Ecg
 *
 * @apiHeader {String} patientid Users id of patient
 * @apiParam {String} startDate The start time for search
 * @apiParam {String} endDate The end Time for search
 *
 * @apiSuccess (200) {array} fileList A list of time that has ecg data
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden User dont have access right to patientid's ecg data
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */

app.post('/rawEcgList/:patientid(\\d+)', verifyToken,
  permission.checkPidToDid(function(req, res, next){
    if (!req.body.startDate || !req.body.endDate) {
      return res.sendStatus(400);
    }
    var sd = new Date(req.body.startDate);
    var ed = new Date(req.body.endDate);
    if (sd === NaN || ed === NaN) {
      return res.sendStatus(400);
    }
    rawEcg.getRawList(req.params.patientid, sd, ed, function(err, result) {
      if (err) {
        return next(err);
      }
      res.send(result);
    });
  })
);


module.exports = app;
