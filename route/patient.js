// express router
var app = require('express').Router(); //eslint-disable-line new-cap

// verifyToken
var verifyToken = require('../middleware/verifyToken');
// permission
var permission = require('../model/permission');

var patient = require('../model/patient');

/*eslint-disable */
/**
 * @api {post} /api/patient/doctorList Get Doctor List
 * @apiName Doctor List
 * @apiGroup Patient
 * @apiPermission Patient
 * @apiDescription Get all doctor that have access right to paient ecg data
 * and doctor who wait to comfirm for access right
 *
 *
 * @apiSuccess {String} did User id of the Doctor
 * @apiSuccess {String} name  Display Name of the Patient.
 * @apiSuccess {String="0","1"} isComfirm O mean the Doctor can access to you ecg data
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "did": "1",
 *       "name": "Owen",
 *       "isComfirm": "0"
 *     },
 *     {
 *       "did": "2",
 *       "name": "that guy",
 *       "isComfirm": "0"
 *     }]
 *
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The user loginned is not Patient
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */
app.post('/doctorList', verifyToken,
  permission.patientOnly(function(req, res, next) {
    patient.getAllDoctor(req.user.pid, function(err, result) {
      if (err) {
        return next(err);
      }
      return res.send(result);
    });
  })
);

/*eslint-disable */
/**
 * @api {post} /api/patient/comfirmDoctor Allow doctor to access
 * @apiName Comfirm doctor
 * @apiGroup Patient
 * @apiPermission Patient
 * @apiDescription Allow Doctor to access ecg data
 *
 * @apiParam {String} did User id of the Doctor
 *
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The user loginned is not Patient
 * @apiError (404) NotFound The doctor has not add to patient list yet
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */

app.post('/comfirmDoctor', verifyToken,
  permission.patientOnly(function(req, res, next) {
    if (!req.body.did) {
      return res.sendStatus(400);
    }
    patient.comfirmDoctor(req.body.did, req.user.pid, function(err, result) {
      if (err) {
        return next(err);
      } else if (result.affectedRows <= 0) {
        return res.sendStatus(404);
      }
      return res.sendStatus(200);
    });
  })
);

module.exports = app;
