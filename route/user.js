
// express router
var app = require('express').Router(); //eslint-disable-line new-cap
var crypto = require('crypto');
var verifyRecaptcha = require('../middleware/verifyRecaptcha');

var jwt = require('jsonwebtoken');
var secret = require('../config').jwt.secret;
var jwtOptions = {
  algorithm: 'HS256'
};

// verifyToken
var verifyToken = require('../middleware/verifyToken');
var permission = require('../model/permission');

// user model
var user = require('../model/user');
/**
 * @api {post} /api/user/register User Registration
 * @apiName Register
 * @apiGroup User
 *
 * @apiParam {String} username Users unique username for login.
 * @apiParam {String} password Users login password.
 * @apiParam {String} email (Optional) User email for forgetting password
 * @apiParam {String} phone (Optional) User name phone number for doctor calling
 * @apiParam {String} displayName (Optional) Username displayname
 * @apiParam {String} recaptcha recaptcha code given by google
 * @apiParam {String="doctor","patient"} type Users type.
 * @apiParamExample {json} Request-Example:
 *     {
 *       "username": "demo",
 *       "password": "demo",
 *       "type": "doctor"
 *     }
 *
 * @apiSuccess (200) Success
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (409) Duplicated The username has been used.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
app.post('/register', verifyRecaptcha, function(req, res, next) {
  // check field
  if (!req.body.username || !req.body.password ||
        !req.body.type) {
    return res.sendStatus(400);
  }
  if (!(req.body.type === 'patient') && !(req.body.type === 'doctor')) {
    return res.sendStatus(400);
  }
  var valueSet = {};
  var salt = crypto.randomBytes(48).toString('base64');
  var hash = crypto.createHash('sha256')
    .update(salt + req.body.password).digest('hex');
  valueSet.name = req.body.username;
  valueSet.salt = salt;
  valueSet.hash = hash;
  valueSet.type = req.body.type;
  valueSet.email = req.body.email || null;
  valueSet.displayName = req.body.displayName || null;
  valueSet.phone = req.body.phone || null;
  user.create(valueSet, function(err) {
    if (err && err.code === 'ER_DUP_ENTRY') {
      return res.sendStatus(409);
    } else if (err) {
      return next(err);
    }
    res.sendStatus(200);
  });
});

/*eslint-disable */
/**
 * @api {put} /api/user/profile Change User profile
 * @apiName Change user profile
 * @apiGroup User
 *
 * @apiParam {String} email (Optional) User email for forgetting password
 * @apiParam {String} phone (Optional) User name phone number for doctor calling
 * @apiParam {String} displayName (Optional) Username displayname
 * @apiParam {String} age (Optional) Username age
 * @apiParam {String} weight (Optional) Username weight
 * @apiParam {String} height (Optional) Username height
 * @apiParamExample {json} Request-Example:
 *     {
 *       "email": "myNameIsOwen@clever.com",
 *       "phone": "1239 9999",
 *       "displayName": "genius"
 *     }
 *
 * @apiSuccess (200) Success
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiParam {String} email (Optional) User email for forgetting password
 * @apiError (403) Forbidden The old password is incorrect or user doesnt exist anymore.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
/*eslint-enable */
app.put('/profile', verifyToken, function(req, res, next) {
  // check field
  var setValues = {};
  if (req.body.email) {
    setValues.email = req.body.email;
  }
  if (req.body.phone) {
    setValues.phone = req.body.phone;
  }
  if (req.body.displayName) {
    setValues.displayName = req.body.displayName;
  }
  if (req.body.age) {
    setValues.age = req.body.age;
  }
  if (req.body.weight) {
    setValues.weight = req.body.weight;
  }
  if (req.body.height) {
    setValues.height = req.body.height;
  }
  if (Object.keys(setValues).length <= 0) {
    return res.sendStatus(400);
  }
  user.changeProfile(req.user.pid, setValues, function(err) {
    if (err) {
      return next(err);
    }
    return res.sendStatus(200);
  });
});
/*eslint-disable */
/**
 * @api {GET} /api/user/profile Get Current User profile
 * @apiName Get user profile
 * @apiGroup User
 *
 *
 * @apiSuccess (200) Success
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The old password is incorrect or user doesnt exist anymore.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
/*eslint-enable */
app.get('/profile', verifyToken, function(req, res, next) {
  // check field
  user.getByPid(req.user.pid, function(err, result) {
    if (err) {
      return next(err);
    } else if (result.length <= 0) {
      return res.sendStatus(403);
    }
    delete result[0].salt;
    delete result[0].hash;
    delete result[0].resetCode;
    return res.send(result[0]);
  });
});
/*eslint-disable */
/**
 * @api {GET} /api/user/profile/:pid Get Specific User profile
 * @apiName Get Specific user profile
 * @apiGroup User
 *
 * @apiHeader {String} pid Users id of patient
 *
 * @apiSuccess (200) Success
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The old password is incorrect or user doesnt exist anymore.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
/*eslint-enable */
app.get('/profile/:patientid(\\d+)', verifyToken,
  permission.checkPidToDid(function(req, res, next) {
// check field completenesss
    user.getByPid(req.params.patientid, function(err, result) {
      if (err) {
        return next(err);
      } else if (result.length <= 0) {
        return res.sendStatus(403);
      }
      delete result[0].salt;
      delete result[0].hash;
      delete result[0].resetCode;
      return res.send(result[0]);
    });
  })
);

/**
 * @api {post} /api/user/login Login
 * @apiName Login
 * @apiGroup User
 *
 * @apiParam {String} username Users Login username,
 * @apiParam {String} password Users login password.
 * @apiParam {String="doctor","patient"} [type] Optional Users type.
 * @apiParamExample {json} Request-Example:
 *     {
 *       "username": "demo",
 *       "password": "demo",
 *       "type": "doctor"
 *     }
 *
 * @apiSuccess {String} username display name of the User.
 * @apiSuccess {String} pid  User id.
 * @apiSuccess {String="doctor","patient"} type Users type.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "username": "demo",
 *       "pid": "1",
 *       "type": "doctor",
 *     }
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (404) UserNotFound password or Username is incorrect.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
app.post('/login', function(req, res, next) {
  // field check
  if (!req.body.username || !req.body.password) {
    return res.sendStatus(400);
  }
  var username = req.body.username;
  var password = req.body.password;
  var type = req.body.type;
  user.getByNameType(username, function(err, result) {
    if (err) {
      return next(err);
    } else if (result.length <= 0) {
      return res.sendStatus(404);
    }
    if (req.body.type) {
      if (result[0].type !== type) {
        return res.sendStatus(404);
      }
    }
    var hash = crypto.createHash('sha256')
            .update(result[0].salt + password).digest('hex');
    if (hash === result[0].hash) {
      var payload = {
        'pid': result[0].pid,
        'name': result[0].name,
        'type': result[0].type,
        'time': new Date(),
      };
      // TODO: generate ssh key for scret
      res.cookie('token', jwt.sign(payload, secret, jwtOptions),
        {
          expires  : new Date(Date.now() + 9999999),
          httpOnly : false
        });
      return res.send({username: result[0].name, pid: result[0].pid,
        displayName: result[0].displayName, type: result[0].type});
    }
    res.sendStatus(404);
  });
});

/**
 * @api {post} /api/user/logout Logout
 * @apiName Logout
 * @apiGroup User
 *
 *
 * @apiSuccess (200) Success Successfully logout
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
app.post('/logout', function(req, res) {
  // clear auth token
  res.clearCookie('token');
  res.sendStatus(200);
});

/**
 * @api {post} /api/user/test Login Status Test
 * @apiName Login Test
 * @apiGroup User
 *
 * @apiSuccess (200) Success Success with cookie reply
 * @apiSuccess {String} username display name of the User.
 * @apiSuccess {String} pid  User id.
 * @apiSuccess {String="doctor","patient} type User Type.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "username": "demo",
 *       "pid": "1",
 *       "type": "patient"
 *     }
 *
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
app.post('/test',verifyToken, function(req, res) {
  res.send({username: req.user.name, pid: req.user.pid, type: req.user.type});
});
/*eslint-disable */
/**
 * @api {post} /api/user/changePassword Change User Password
 * @apiName Change Password
 * @apiGroup User
 *
 * @apiParam {String} oldPassword Users old password for double confirm.
 * @apiParam {String} newPassword Users new password.
 * @apiParamExample {json} Request-Example:
 *     {
 *       "oldPassword": "ilovewmh",
 *       "newPassword": "aboveisalie",
 *     }
 *
 * @apiSuccess (200) Success Successfully change the user password.
 *
 * @apiError (400) BadRequest The api Params is not valid or missing.
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The old password is incorrect or user doesnt exist anymore.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */
app.post('/changePassword', verifyToken, function(req, res, next) {
  // check field
  if (!req.body.oldPassword || !req.body.newPassword) {
    return res.sendStatus(400);
  }
  var password = req.body.oldPassword;
  user.getByPid(req.user.pid, function(err, result){
    if (err) {
      return next(err);
    } else if (result.length <= 0) {
      return res.sendStatus(403);
    }
    var hash = crypto.createHash('sha256')
            .update(result[0].salt + password).digest('hex');
    if (hash === result[0].hash) {
      user.changePassword(req.user.pid,
      req.body.newPassword, function(err2) {
        if (err2) {
          return next(err2);
        }
        return res.sendStatus(200);
      });
    } else {
      return res.sendStatus(403);
    }
  });
});


module.exports = app;
