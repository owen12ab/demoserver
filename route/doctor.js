// express router
var app = require('express').Router(); //eslint-disable-line new-cap

// verifyToken
var verifyToken = require('../middleware/verifyToken');
// permission
var permission = require('../model/permission');

var doctor = require('../model/doctor');

/*eslint-disable */
/**
 * @api {post} /api/doctor/patientList Get Patient List
 * @apiName Patient List
 * @apiGroup Doctor
 * @apiPermission Doctor
 *
 *
 * @apiSuccess {String} patientid User id of the patient.
 * @apiSuccess {String} name  Display Name of the Patient, should avoid display it
 * @apiSuccess {String} phone phone number of patient
 * @apiSuccess {String} email email address of patient
 * @apiSuccess {string} displayName displayName of patient
 * @apiSuccess {String="0","1"} isComfirm Patient comfirm status. 0 mean not comfirm and no access right to ecg yet.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       "patientid": "1",
 *       "name": "Owen",
 *       "isComfirm": "1",
 *       "email": "fkinGood@cuhk.is.crap.com"
 *     },
 *     {
 *       "patientid": "2",
 *       "name": "that guy",
 *       "isComfirm": "0",
 *     }]
 *
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The user loginned is not doctor
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */
app.post('/patientList', verifyToken,
  permission.doctorOnly(function(req, res, next) {
    doctor.getAllPatient(req.user.pid, function(err, result){
      if (err) {
        return next(err);
      }
      res.send(result);
    });
  })
);

/*eslint-disable */
/**
 * @api {post} /api/doctor/addPatient Add patient to list
 * @apiName Add Patient
 * @apiGroup Doctor
 * @apiPermission Doctor
 *
 * @apiParam {String} patientid User id of Patient
 *
 * @apiSuccess (200) Success Successfully Add the patient to the list and need to comfirm by patient yet.
 *
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The user loginned is not doctor
 * @apiError (409) Duplicated The patient is already in the list.
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */

app.post('/addPatient', verifyToken,
  permission.doctorOnly(function(req, res, next) {
    if (!req.body.patientid) {
      return res.sendStatus(400);
    }
    doctor.addPatient(req.user.pid, req.body.patientid, function(err) {
      if (err) {
        return next(err);
      }
      return res.sendStatus(200);
    });
  })
);
/*eslint-disable */
/**
 * @api {post} /api/doctor/removePatient Remove patient from list
 * @apiName Remove Patient
 * @apiGroup Doctor
 * @apiPermission Doctor
 * @apiDescription Remove Patient to the list
 * No matter the patient is confirm or not.
 *
 * @apiParam {String} patientid User id of Patient
 *
 * @apiSuccess (200) Success Successfully Remove the patient to the list.
 *
 * @apiError (401) Unauthorized The user has not been login yet.
 * @apiError (403) Forbidden The user loginned is not doctor
 * @apiError (500) InternelServerError Unexpected server side error.
 *
 */
 /*eslint-enable */

app.post('/removePatient', verifyToken,
  permission.doctorOnly(function(req, res, next) {
    if (!req.body.patientid) {
      return res.sendStatus(400);
    }
    doctor.removePatient(req.user.pid, req.body.patientid, function(err) {
      if (err) {
        return next(err);
      }
      return res.sendStatus(200);
    });
  })
);


module.exports = app;
