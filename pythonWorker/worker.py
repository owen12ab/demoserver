import numpy as np
import scipy.signal
import sys
import zerorpc
import re
import mne.preprocessing as mnePre
import mne

from numpy import NaN, Inf, arange, isscalar, asarray, array


class ECGRPC(object):
    def peakdet(self, v, delta, x=None):
        maxtab = []
        mintab = []
        p = [[], [], [], [], [], []]
        q = [[], [], [], [], [], []]
        r = [[], [], [], [], [], []]
        s = [[], [], [], [], [], []]
        t = [[], [], [], [], [], []]
        # p, q, r, s, t = 0, 1, 2, 3, 4
        states = [0, 1, 2, 3, 4, 5]
        thres = 3
        if x is None:
            x = arange(len(v))

        v = asarray(v)

        if len(v) != len(x):
            sys.exit('Input vectors v and x must have same length')

        if not isscalar(delta):
            sys.exit('Input argument delta must be a scalar')

        if delta <= 0:
            sys.exit('Input argument delta must be positive')

        mn, mx = Inf, -Inf
        mnpos, mxpos = NaN, NaN

        lookformax = True
        prev_peak = [9999, 0, 99999, 0, 99999, 0]
        over_mean = np.mean(v)
        for i in arange(len(v)):
            this = v[i]
            if this > mx:
                mx = this
                mxpos = x[i]
            if this < mn:
                mn = this
                mnpos = x[i]

            if lookformax:
                if this < mx - delta:
                    maxtab.append((mxpos, mx))
                    mn = this
                    mnpos = x[i]
                    lookformax = False
                    for index in range(0, len(states)):
                        state = states[index]
                        if state == 0 or state == 2 or state == 4 and np.abs(mx - prev_peak[index]) > thres:
                            prev_peak[index] = mx
                            if state == 0:
                                states[index] = 1
                                q[index].append(mxpos)
                            elif state == 2:
                                prev_peak[index] = mx
                                if mx >= over_mean:
                                    states[index] = 3
                                    s[index].append(mxpos)
                            elif state == 4:
                                states[index] = 5
                        elif state == 1 or state == 3 or state == 5:
                            prev_peak[index] = mx
                            if state == 1:
                                if len(q[index]) == 0:
                                    q[index].append(mxpos)
                                q[index][len(q[index]) - 1] = mxpos
                            elif state == 3:
                                states[index] = -1
            else:
                if this > mn + delta:
                    mintab.append((mnpos, mn))
                    mx = this
                    mxpos = x[i]
                    lookformax = True
                    for index in range(0, len(states)):
                        state = states[index]
                        if state == 0 or state == 2 or state == 4:
                            if np.abs(mn - prev_peak[index]) > thres * 3:
                                states[index] = -1
                        elif state == 1 or state == 3 or state == 5:
                            if np.abs(mn - prev_peak[index]) > thres * 2:
                                prev_peak[index] = mn
                                if state == 1:
                                    states[index] = 2
                                    r[index].append(mnpos)
                                elif state == 3:
                                    states[index] = 4
                                    t[index].append(mnpos)
                                elif state == 5:
                                    states[index] = 0
                                    p[index].append(mnpos)
        return_arr = [[], [], [], [], []]
        for index in range(0, len(states)):
            if states[index] != -1:
                return_arr[0].append(p[index])
                return_arr[1].append(q[index])
                return_arr[2].append(r[index])
                return_arr[3].append(s[index])
                return_arr[4].append(t[index])
        return array(maxtab), array(mintab), return_arr

    def peakdet1(self, ecg):
        raw = mne.io.RawArray([ecg], mne.create_info(ch_names=['raz'], sfreq=100, ch_types=['mag']))
        ecg_events, name_c, _ = mnePre.find_ecg_events(raw)
        return ecg_events[:, 0]

    def analyseEcg(self, ecgStr):
        ecg = re.findall('...', ecgStr)
        tmp = np.zeros(len(ecg))
        for i in range(0, len(ecg)):
            tmp[i] = int(ecg[i], 16)
        fs = 3
        f1 = 0.5
        f2 = 45
        Wn = [f1 * 2 / fs, f2 * 2 / fs]
        N = 3.0
        # b, a = scipy.signal.butter(N, Wn)
        # ecg = scipy.signal.filtfilt(b, a, ecg)
        # peaks, minP, ecg_det = self.peakdet(ecg, 2)
        ecg_det = self.peakdet1(tmp)
        ret = []
        for a in ecg_det:
            ret.append(a)
        return ret

    def test(self):
        return "hello"

server = zerorpc.Server(ECGRPC())
server.bind("tcp://0.0.0.0:4001")
server.run()
