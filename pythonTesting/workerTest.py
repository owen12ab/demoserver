import numpy as np
import scipy.signal
import sys
import re
import cv2
import mne.preprocessing as mnePre
import mne
from numpy import NaN, Inf, arange, isscalar, asarray, array


def peakdet(v, delta, x=None):
    maxtab = []
    mintab = []
    p = [[], [], [], [], [], []]
    q = [[], [], [], [], [], []]
    r = [[], [], [], [], [], []]
    s = [[], [], [], [], [], []]
    t = [[], [], [], [], [], []]
    # p, q, r, s, t = 0, 1, 2, 3, 4
    states = [0, 1, 2, 3, 4, 5]
    thres = 3
    if x is None:
        x = arange(len(v))

    v = asarray(v)

    if len(v) != len(x):
        sys.exit('Input vectors v and x must have same length')

    if not isscalar(delta):
        sys.exit('Input argument delta must be a scalar')

    if delta <= 0:
        sys.exit('Input argument delta must be positive')

    mn, mx = Inf, -Inf
    mnpos, mxpos = NaN, NaN

    lookformax = True
    prev_peak = [9999, 0, 99999, 0, 99999, 0]
    over_mean = np.mean(v)
    for i in arange(len(v)):
        this = v[i]
        if this > mx:
            mx = this
            mxpos = x[i]
        if this < mn:
            mn = this
            mnpos = x[i]

        if lookformax:
            if this < mx - delta:
                maxtab.append((mxpos, mx))
                mn = this
                mnpos = x[i]
                lookformax = False
                for index in range(0, len(states)):
                    state = states[index]
                    if state == 0 or state == 2 or state == 4 and np.abs(mx - prev_peak[index]) > thres:
                        prev_peak[index] = mx
                        if state == 0:
                            states[index] = 1
                            q[index].append(mxpos)
                        elif state == 2:
                            prev_peak[index] = mx
                            if mx >= over_mean:
                                states[index] = 3
                                s[index].append(mxpos)
                        elif state == 4:
                            states[index] = 5
                    elif state == 1 or state == 3 or state == 5:
                        prev_peak[index] = mx
                        if state == 1:
                            if len(q[index]) == 0:
                                q[index].append(mxpos)
                            q[index][len(q[index]) - 1] = mxpos
                        elif state == 3:
                            states[index] = -1
        else:
            if this > mn + delta:
                mintab.append((mnpos, mn))
                mx = this
                mxpos = x[i]
                lookformax = True
                for index in range(0, len(states)):
                    state = states[index]
                    if state == 0 or state == 2 or state == 4:
                        if np.abs(mn - prev_peak[index]) > thres * 3:
                            states[index] = -1
                    elif state == 1 or state == 3 or state == 5:
                        if np.abs(mn - prev_peak[index]) > thres * 2:
                            prev_peak[index] = mn
                            if state == 1:
                                states[index] = 2
                                r[index].append(mnpos)
                            elif state == 3:
                                states[index] = 4
                                t[index].append(mnpos)
                            elif state == 5:
                                states[index] = 0
                                p[index].append(mnpos)
    return_arr = [[], [], [], [], []]
    for index in range(0, len(states)):
        if states[index] != -1:
            return_arr[0].append(p[index])
            return_arr[1].append(q[index])
            return_arr[2].append(r[index])
            return_arr[3].append(s[index])
            return_arr[4].append(t[index])
    return array(maxtab), array(mintab), return_arr


def analyseEcg(ecgStr, out):
    ecg = re.findall('...', ecgStr)
    tmp = np.zeros((len(ecg)), dtype=np.int)
    img = np.full((1000, len(ecg), 3), 255)
    prev_x = 0
    prev_y = 0
    print len(ecg)
    print "writting image file"
    for i in range(0, len(ecg)):
        tmp[i] = int(ecg[i], 16)
        if prev_x != 0:
            cv2.line(img, (prev_x, prev_y), (prev_x + 1, tmp[i]), (255, 0, 0))
        prev_x = prev_x + 1
        prev_y = tmp[i]
    cv2.imwrite('normal-' + out + '.jpg', img)
    ecg = tmp
    fs = 3
    f1 = 0.5
    f2 = 45
    Wn = [f1 * 2 / fs, f2 * 2 / fs]
    N = 3.0
    b, a = scipy.signal.butter(N, Wn)
    ecg = scipy.signal.filtfilt(b, a, ecg)
    prev_x = 0
    prev_y = 0
    print "writting noice cancellation image"
    img = np.full((1000, len(ecg), 3), 255)
    for i in range(0, len(ecg)):
        if prev_x != 0:
            cv2.line(img, (prev_x, prev_y), (prev_x + 1, int(ecg[i])), (255, 0, 0))
        prev_x = prev_x + 1
        prev_y = int(ecg[i])
    cv2.imwrite('noiceCal-' + out + '.jpg', img)
    raw = mne.io.RawArray([ecg], mne.create_info(ch_names=['raz'], sfreq=100, ch_types=['mag']))
    ecg_events, name_c, _ = mnePre.find_ecg_events(raw)
    print ecg_events
    for i in ecg_events[:, 0]:
        cv2.circle(img, (i, int(ecg[i])), 5, (150, 150, 0))
    cv2.imwrite('result-' + out + '.jpg', img)
    print name_c
    # peaks, minP, ecg_det = peakdet(ecg, 2)
    # print ecg_det[2][0]
    # print ecg_det[2]
    # return [ecg_det, len(ecg)]

farr = ["a01", "a02r", "a03er", "a010", "a12", "b02", "b04", "c01", "x01", "x05", "x16", "x21", "x28", "amm3", "amm4a"]
for f in farr:
    path = "out_" + f + ".data"
    finput = open(path, "rb")
    output = f
    data = finput.read()
    lines = data.splitlines()
    dataStr = ''
    print "loading file"
    for line in lines:
        # print line
        dataStr += line.split('--')[1]
    try:
        analyseEcg(dataStr, output)
    except:
        print "error"
    finput.close()
