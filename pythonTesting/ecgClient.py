import requests
import json
import threading
import sys
import re
from datetime import datetime
from socketIO_client import SocketIO, BaseNamespace


def on_echo(self):
    print "echo"


def on_connect(self):
    print('[Connected]')


def on_disconnect(self):
    print "disconnect"

headers = {'content-type': 'application/json'}
url = 'http://projgw.cse.cuhk.edu.hk:2874/api/user/login'

data = {"username": "demo2", "password": "demo2", "type": "patient"}
r = requests.post(url, data=json.dumps(data), headers=headers)
print r.status_code
print r.cookies['token']
socketIO = SocketIO('http://projgw.cse.cuhk.edu.hk:2874/api/ws', 2874, cookies={'token': r.cookies['token']})
socketIO.on("echo", on_echo)
socketIO.emit("echo")
f = sys.argv[1]
path = "out_" + f + ".data"
finput = open(path, "rb")
data = finput.read()
lines = data.splitlines()
dataStr = ""
for line in lines:
    dataStr += line.split('--')[1]
ecg = re.findall('...', dataStr)


def sendEcg(lcount):
    dt = datetime.now()
    dtf = dt.strftime('%Y-%m-%d %H:%M:%S.%f')
    print "".join(ecg[lcount: lcount + 100])
    socketIO.emit("relayEcg", dtf + '--' + ''.join(ecg[lcount: lcount + 100]))
    lcount += 100
    if lcount < len(ecg):
        if lcount % 500 == 0 and lcount != 0:
            socketIO.emit("newEcg", dtf + '--' + "".join(ecg[lcount - 500: lcount]))
        t = threading.Timer(1.0, sendEcg, [lcount])
        t.start()

print "start?"
t = threading.Timer(1.0, sendEcg, [0])
t.start()

socketIO.wait()
