

def readImageFile(fname):
    finput = open("samples_" + fname + ".csv", "rb")
    foutput = open("out_" + fname + ".data", "a")
    data = finput.read()
    lines = data.splitlines()
    foutput.write("aaaa--")
    for lineNum in range(len(lines)):
        if lineNum >= 2:
            data = lines[lineNum].split(",")
            value = int(float(data[1]) * 250 + 500)
            foutput.write("%03x" % value)
    foutput.write("\n")
    finput.close()
    foutput.close()

farr = ["a01", "a02r", "a03er", "a010", "a12", "b02", "b04", "c01", "x01", "x05", "x16", "x21", "x28", "amm3", "amm4a"]
for f in farr:
    readImageFile(f)
