
var config = require('../config');

var mysql = require('mysql');
var pool = mysql.createPool(config.fypMySQL);

module.exports = pool;
