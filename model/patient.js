var patient = {};
var pool = require('./fypDB');

patient.getAllDoctor = function(patientid, callback) {
  pool.query('SELECT did, name, isComfirm FROM profile JOIN doctorOf \
    ON pid = did WHERE patientid = ?',
    [patientid],
    callback);
};

patient.comfirmDoctor = function(doctorid, patientid, callback) {
  pool.query('UPDATE doctorOf SET isComfirm = 1 \
    WHERE did = ? AND patientid = ?',
    [doctorid, patientid],
    callback);
};

module.exports = patient;
