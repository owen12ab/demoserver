var ecg = {};
var pool = require('./fypDB');
ecg.add = function(pid, data, time, callback) {
  pool.query('INSERT INTO ecg(pid, rp, rq, rr, rs, rt, time) \
    VALUES (?,?,?,?,?,?,?)',
    [pid, data[0], data[1], data[2], data[3], data[4], time],
    callback);
};
ecg.addRR = function(pid, data, time, callback) {
  pool.query('INSERT INTO ecg(pid, rr, time) \
    VALUES (?,?,?)',
    [pid, data, time],
    callback);
};
ecg.getByPidTime = function(pid, startTime, endTime, callback) {
  pool.query('SELECT * FROM ecg WHERE pid = ? AND time BETWEEN ? AND ?',
    [pid, startTime, endTime],
    callback);
};

ecg.getByPid = function(pid, callback) {
  pool.query('SELECT * FROM ecg WHERE pid = ?',
    [pid],
    callback);
};

ecg.getByPidDate = function(pid, startTime, endTime, callback) {
  pool.query('SELECT * FROM ecg WHERE pid = ?\
  AND (time BETWEEN ? AND ?)',
  [pid, startTime, endTime],
  callback);
};

ecg.getAvgByPid = function(pid, callback) {
  pool.query('SELECT AVG(rp) as rp, AVG(rq) as rq, AVG(rr) as rr, \
    AVG(rs) as rs, AVG(rt) as rt\
    FROM ecg WHERE pid = ?',
    [pid],
    callback);
};
module.exports = ecg;
