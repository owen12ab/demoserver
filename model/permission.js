var permission = {};

//var user = require('./user');
var doctor = require('./doctor');

permission.checkPidToDid = function(fn) {
  return function(req, res, next) {
    var patientId = parseInt(req.params.patientid);
    if (parseInt(patientId) === req.user.pid) {
      fn(req, res, next);
    } else {
      doctor.getAllPatient(req.user.pid, function(err, results) {
        if (err) {
          return next(err);
        }
        var exist = false;
        results.forEach(function(result){
          if (result.patientid === patientId) {
            exist = true;
          }
        });
        if (exist) {
          fn(req, res, next);
        } else {
          res.sendStatus(403);
        }
      });
    }
  };
};

permission.doctorOnly = function(fn) {
  return function(req, res, next) {
    if (req.user.type && req.user.type === 'doctor') {
      fn(req,res, next);
    } else {
      return res.sendStatus(403);
    }
  };
};

permission.patientOnly = function(fn) {
  return function(req, res, next) {
    if (req.user.type && req.user.type === 'patient') {
      fn(req, res, next);
    } else {
      return res.sendStatus(403);
    }
  };
};

module.exports = permission;
