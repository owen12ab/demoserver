var user = {};
var pool = require('./fypDB');

var crypto = require('crypto');

user.create = function(setValue, callback) {
  pool.query('INSERT INTO profile SET ?',
    setValue,
    callback);
};

user.changePassword = function(pid, password, callback) {
  var salt = crypto.randomBytes(48).toString('base64');
  var hash = crypto.createHash('sha256').update(salt +
    password).digest('hex');
  pool.query('UPDATE profile SET hash = ?, salt =? WHERE pid = ?',
    [hash, salt, pid],
    callback);
};

user.getByPid = function(pid, callback) {
  pool.query('SELECT * FROM profile WHERE pid = ?',
    [pid],
    callback);
};

user.getByNameType = function(username, callback) {
  pool.query('SELECT * FROM profile WHERE name = ?',
    [username],
    callback);
};

user.changeProfile = function(pid, setValues, callback) {
  pool.query('UPDATE profile SET ? WHERE pid = ?',
    [setValues, pid],
    callback);
};

module.exports = user;
