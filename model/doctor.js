var doctor = {};
var pool = require('./fypDB');

doctor.getAllPatient = function(doctorId, callback) {
  pool.query('SELECT patientid, name, displayName, email, phone, \
    isComfirm FROM profile JOIN doctorOf \
    ON pid = patientid WHERE did = ?',
    [doctorId],
    callback);
};

doctor.addPatient = function(doctorId, patientid, callback) {
  pool.query('INSERT INTO doctorOf (patientid, did) VALUES (?,?)',
  [patientid, doctorId],
  callback);
};

doctor.removePatient = function(doctorId, patientid, callback) {
  pool.query('DELETE FROM doctorOf WHERE patientid = ? AND did = ?',
  [patientid, doctorId],
  callback);
};

module.exports = doctor;
