
var rawEcg = {};
var pool = require('./fypDB');
var path = require('../config').rawEcg.path;
var fs = require('fs');

rawEcg.appendToFile = function(pid, receiveDate, data, callback) {
  var minute = receiveDate.getMinutes();
  minute = Math.floor(minute / 5) * 5;
  receiveDate.setSeconds(0);
  receiveDate.setMinutes(minute);
  var fileName = pid + '-' + receiveDate.getFullYear() +
   '-' + receiveDate.getMonth();
  fileName += '-' + receiveDate.getDate() + '-' + receiveDate.getHours() + '-';
  fileName += minute;
  fs.stat(path + fileName, function(err) {
    if (err && err.code === 'ENOENT') {
      // file does not existA
      rawEcg.createNewFile(pid, receiveDate, function(err) {
        if (err) {
          console.log(err);
        }
      });
    }
    fs.appendFile(path + fileName, data, function(err) {
      callback(err);
    });
  });
};

rawEcg.createNewFile = function(pid, RecordTime, callback) {
  pool.query('INSERT INTO rawEcg (patientid, RecordTime) VALUES (?, ?)',
  [pid, RecordTime],
  callback);
};

rawEcg.getRawByDate = function(pid, receiveDate, callback) {
  var minute = receiveDate.getMinutes();
  minute = Math.floor(minute / 5) * 5;
  receiveDate.setSeconds(0);
  receiveDate.setMinutes(minute);
  var fileName = pid + '-' + receiveDate.getFullYear() +
   '-' + receiveDate.getMonth();
  fileName += '-' + receiveDate.getDate() + '-' + receiveDate.getHours() + '-';
  fileName += minute;
  fs.lstat(path + fileName, function(err, stats) {
    callback(err, stats, path + fileName);
  });
};

rawEcg.getRawList = function(pid, startDate, endState, callback) {
  pool.query('SELECT RecordTime FROM rawEcg WHERE \
  RecordTime BETWEEN ? AND ? AND patientid = ?',
  [startDate, endState, pid],
  callback);
};

module.exports = rawEcg;
