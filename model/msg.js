
var msg = {};
var pool = require('./fypDB');

msg.getAll = function(pid, callback) {
  pool.query('SELECT * FROM msg WHERE (fromPid = ? OR toPid = ?)',
  [pid],
  callback
  );
};

msg.getPid = function(pid, withPid, callback) {
  pool.query('SELECT * FROM msg WHERE \
  (fromPid = ? OR toPid = ?) AND (fromPid = ? OR toPid = ?)',
  [pid, pid, withPid, withPid],
  callback
  );
};

msg.save = function(fromPid, toPid, date, msg, callback) {
  pool.query('INSERT INTO msg (fromPid, toPid, mdate, content)',
  [fromPid, toPid, date, msg],
  callback
  );
};

module.exports = msg;
