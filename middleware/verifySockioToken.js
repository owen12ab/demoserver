// middleware for verifying JSON web token in socket.io
var jwt = require('jsonwebtoken');
var jwtSecret = require('../config').jwt.secret;
var jwtVerifyOptions = {
  algorithms: ['HS256']
};
// socket.request.token defined only if it is verified
function verifySocketToken(socket, next) {
  socket.request.token = undefined;
  try {
    if (socket.request.headers.authorization &&
      socket.request.headers.authorization.split(' ')[0] === 'Bearer') {
      socket.request.token =
        jwt.verify(socket.request.headers.authorization.split(' ')[1],
          jwtSecret, jwtVerifyOptions);
    } else if (socket.request.cookies.token) {
      socket.request.token =
        jwt.verify(socket.request.cookies.token, jwtSecret, jwtVerifyOptions);
    }
  } catch (err) {
    socket.request.token = undefined;
  }
  next();
}
module.exports = verifySocketToken;
