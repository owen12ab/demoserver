
var request = require('request');
var verifyRecaptcha = function(req, res, next) {
  if (req.body.recaptcha) {
    if (req.body.recaptcha === '') {
      return res.sendStatus(400);
    }
    var recaptcha = req.body.recaptcha;
    var secretKey = '6Lf7ZQwUAAAAAFsW9MPMF73bVV1y0ndQiwbvZRo9';
    var verificationUrl =
      'https://www.google.com/recaptcha/api/siteverify?secret='
      + secretKey + '&response='
      + recaptcha;
    request(verificationUrl,function(error,response,body) {
      body = JSON.parse(body);
      if (body.success !== undefined && !body.success) {
        return res.sendStatus(400);
      }
      // pass to next middleware or endpoint
      next();
    });
  } else {
    // pass to next middleware or endpoint
    next();
  }
};

module.exports = verifyRecaptcha;
