
var config = require('../config');

var verifyToken = require('express-jwt')({
  algorithms: ['HS256'],
  secret: config.jwt.secret,
  getToken: function fromHeaderOrCookie(req) {
    if (req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'Bearer') {
      return req.headers.authorization.split(' ')[1];
    } else if (req.cookies && req.cookies.token) {
      return req.cookies.token;
    }
    return null;
  }
});

module.exports = verifyToken;
