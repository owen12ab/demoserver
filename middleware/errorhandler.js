
var handler =
function(err, req, res, next) {//eslint-disable-line no-unused-vars
  if (err && err.name &&
      err.name === 'UnauthorizedError') {
    return res.status(401).send('Unauthorized');
  } else if (err && err.code &&
    err.code === 'ER_DUP_ENTRY') {
    return res.status(409).send('duplicated');
  } else if (err && err.code && err.code &&
    err.code === 'ENOENT') {
    return res.status(404).send('NotFound');
  } else if (err.name && err.name === 'SyntaxError') {
    return res.sendStatus(400);
  }
  console.log(err);
  res.status(500).send('Something broken');
};

module.exports = handler;
